- Bootstrapping
- ![](chapter6-img1.png)
  - Typical bootstrapping actions:
    - mounts drives
    - start services
    - Update files
    - Get latest version of software
    - Register with ELB
  - You can use cloud-init and EC2 user data to customize an EC2 instance at launch time
  - But with CF you can perform even more complex and detailed bootstrapping
  - Use the AWS CF template to define the set of packages, files, and OS services for EC2
    - Metadata:
      - EC2 provides an instance metadat store that can be accessed loally from the instance.
      - You can use the data stored in the instance metadata store to further customize AMIs while the EC2 instance is being launched
    - Helper Scripts:
      - Build on cloud-init functionality so you can create a common cloud-init startup script that's driven from the template
      - Describe what apps need to be installed on the host in the template
        - cfn-init
          - Retrieves and interprets the resource metadata to install packages, create files, and start services
        - cfn-signal
          - A simple wrapper to signal an AWS CF WaitCondition for synchronizing other resources in the stack when the application is ready
        - cfn-get-metadata
          - A wrapper script that retrieves either all metadata that's defined for a resource or path to a specifiy key or a sub-tree of the resource metadata
        - cfn-hup
          - A daemon that checks for updates to metadata and executes custom hooks when changes are detected.
- Wait Conditions
  - They pause the execution of stack creation and wait for a number of success signals before continuing stack creation
  - Typical use cases:
    - To coordinate stack resource creation with configuration actions that are external to the stack cretion (hybrid scenarios)
    - Wait on the provisioning of resources
    - Wait for an RDS DB to be configured
    - Wait for a NAT instance to be configured before private instances attempt to access the internet
  - You can use the wait condition to make AWS CF pause the creation of a stack and wait for the signal.
  - Wait Conditon Properties
    - Count:
      - \# of success signals that CF must receive before it continues the stack creation process
    - Handle
      - A refernce to the wait condition handle used to signal this wait condition
    - Timeout
      - The length of time (in seconds) to wait for the \# of signals that athe count property specifies
- Creation Policies
  - Puase the creation of a resource until a requisite \/# of success signals are received
    - Creating EC2 instances, but they also require applications (Nginx), to be installed
    - Without creation policy, the EC2 instance would appear ready before the app is installed
    - Just as with a wait condition, you specify the timeout and \# of success signals
    - You attach a creation policy to a resource in the stack

        ```
        "CreationPolicy": {
            "ResourceSignals": {
                "Count": "3",
                "Timeout": "PT15M"
            }
        }
        ```

  - Creation Policies and wait conditions can be used together
- Update Policies
  - Provides a structured way of updating instances in ASG without downtime
    - Need to be able to update launch configurations without causing downtime and update all instance in ASG
    - **UpdatePolicy** attribute: Can be associated with an ASG in a template and handle updates to it
    - 3 types of updates
      - AutoScalingReplacingUpdate
        - Specifies whether to replace an instance in an ASG or the whole ASG using the boolean ***WillReplace**
        - Old ASG is retained in case of rollback
      - AutoScalingRollingUpdate
        - Controls how many instances in an ASG are updated at one time
      - AutoScalingScheduledAction
        - Can be used when you have predictable load patterns
        - Can be used to handle ASG updates for MinSize, MaxSize, and DesiredCapacity
- Helper Scripts
  - A set of Python scripts to install software and start services on EC2 instances
    - called directly from template
    - work with resource metadata defined in template
    - Pre-installed on the latest version of the Amazon Linux AMI
    - 4 helper scripts: cfn-init, cfn-signal, cfn-get-metadata, cfn-hup
    - You must include calls to execute helper scripts
  - Use cases
    - Senc signals back to a stack
    - Configure and bootstrap instances
    - Update instances

```
cfn-init --stack | -s stack.name.or.id \
    --resource | -r logical.resource.id \
    --regioon region
    --access-key access.key \
    --secret-key secret.key \
    --role rolename \
    --credential-file | -f credential.file \
    --configsets | -c config.sets \
    --url | -u service.url \
    --http-proxy HTTP.proxy \
    --https-proxy HTTPS.proxy \
    --verbose | -v
```

- cfn-signal

```
cfn-signal --success | -s signal.to.send \
    --access-key access.key \
    --credential-file | -f credential.file \
    --exit-code | -e exit.code \
    --http-proxy HTTP.proxy \
    --https-proxy HTTPS.proxy \
    --id | -i unique.id \
    --region AWS.region \
    --resource resource.logical.ID \
    --role IAM.role.name \
    --secret-key secret.key \
    --stak stack.name.or.stack.ID \
    --url AWS CloudFormation.endpoint
```

- cfn-get-metadata

```
cfn-get-metadata --access-key access.key \
    --secret-key secret.key \
    --credential-file | -f credential.file \
    --key | -k key \
    --stack | -s stack.namer.or.id \
    --resource | -r logical.resource.id \
    --role IAM.role.name \
    --url -u service.url \
    --region region
```

- cfn-hup

```
cfn-hup --config | -c config.dir \ # Identifies a config directory path
    --no-daemon \                   # Use this if you want to run it once
    --verbose | -v                  # Returns details about the command
```

- Helper Script Walkthrough
![](chapter6-img2.png)
- Sys Manager Parameter Store with CloudFormation
  - Parameters allow customization of templates
  - Parameters make your template code dynamically configurable, improving the reusability of your code
  - In late 2017 AWS announced the integration of CF with AWS System Manager Parameter Store
  - Can abstract values out of our templates, making them more secure and promoting reusability
  - How it works:
    - Can use the existing Parameter section of your CF template to define System Manager parameters, along with other parameters
    - The value for this type of parameter would be the SSM parameter key instead of a string or other value
  - Available parameters:
    - AWS::SSM::Parameter::Name
    - AWS::SSM::Parameter::Value<String>
    - AWS::SSM::Parameter::Value<List<String>>
    - AWS::SSM::Parameter::Value<Any AWS type>
- Dynamic References in CloudFormation Templates
  - provides a compact, powerful way of your specify external values that are stored and managed in other services (SSM Parameter Store) in a template
  - When you use a dynamic refernece, CF retrieves the value of the specified reference when necessary during stack and change set operations
  - CF currently supports the following dynmaic reference patterns:
    - **ssm**
      - for plaintext values stored in System Manager Parameter Store
    - **ssm-secure**
      - for secure strings stored in Systems Manager Paramter Store
    - **secretmanager** for entire secrets or specified secret values that are stored in AWS secrets manager
  - You can include up to 60 dynamic references in a template
  - Dynamic references adhere to the following pattern (for ssm)
    - "{{resolve:ssm:paramter-name"version}}"
- Secrets Manager
  - makes it easier to rotate, manage, and retrieve database credentials, API Keys, and other secrets throughout their lifecycle
  - You can reference templates to create unique secrets with every invocation of your template
  - ![](chapter6-img3.png)
- Macros
  - 1. Create a Lambda function with macro logic
  - 2. Deploy the Lambda function
  - 3. Register the Lambda function as a macro

```
NewMacro:
  Type:AWS::CloudFormation:Macro
  Properties:
    Name: "use descriptive name"
    FunctionName:
      Fn::GetATTN:
        - LambdaMacro
        - ARN
```

- 4. Use macro throughout your account

```
Transform:
  - NewMacro
```
