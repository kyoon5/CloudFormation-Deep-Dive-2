- Custom Resources
- CF does not support every resource in AWS. What if there are tasks we want but they are not supported? 
  - Custom Resources are implemented using Lambda Functions
    - 1. CF retrieves your package source from S3
    - 2. CF deploys Lambda functions
    - 3. Lambda runs and returns data to CF
    - 4. F deploys other resources
- Lambda
  - a compute platform
  - can run code without servers
    - Not charged for hardwaare, 
    - Only charged for how long the code runs
    - Executes on AWS infrastructure
    - Event-driven
    - Removes all infrastructure complexity and responsbility
  - ![](chapter7-img1.png)
- Example - AMI Selection
  - The template creates a Custom Resource and Lambda Fucntion. The custom Resource invokes the Lambda Function, retrieves the code from S3.
  - The Lambda Function retrieves the proper AMI ID real-time, and the EC2 instance is created.
  - How to select the correct AMI
    - Mappings
    - The template will break if the IDs change
    - Use Lambda function that retrieves AMI ID for instance type/region in real time
    - can also pull the mappings out of the template and store them in Parameter Store
- Example: Password Checker
![](chapter7-img2.png)
  - this creates a single IAM User account
  - the user's password is entered as a parameter to the template
  - But what happens if the password is incorrect?
    - There typically isn't a conform password field
  - A second parameter (confirm password) can be created
    - A lambda function confirms if they match.
- Serverless Application Model
![](chapter7-img4.png)
  - Open-source framework fopr building serverless applications
  - Uses CF syntax
    - Optimized for serverless applications
  - Transform: "AWS::Serverless-2016-10-31" 
    - Using the Transform function, we can include SAM code in our CF Templates.